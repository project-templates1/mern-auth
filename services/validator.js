const Validator = require('validator');
const isEmpty = require('is-empty');

function loginInput(data) {
	const errors = {};

	// Convert empty fields to an empty string so we can use validator functions
	data.email = !isEmpty(data.email) ? data.email : '';
	data.password = !isEmpty(data.password) ? data.password : '';

	// Email checks
	if (Validator.isEmpty(data.email)) {
		errors.email = 'Email field is required';
	} else if (!Validator.isEmail(data.email)) {
		errors.email = 'Email is invalid';
	}
	// Password checks
	if (Validator.isEmpty(data.password)) {
		errors.password = 'Password field is required';
	}

	return {
		errors,
		isValid: isEmpty(errors)
	};
};

function registerInput(data) {
	const allRequired = true;
	const infoErrors = validateUserInfo(data,allRequired);

	const passwordErrors = validatePasswords(data);

	const errors = {...infoErrors, ...passwordErrors};

	return {
		errors,
		isValid: isEmpty(errors)
	};
};

function changePasswordInput(data) {
	let errors = {};

	data.email = !isEmpty(data.email) ? data.email : '';

	// Email checks
	if (Validator.isEmpty(data.email)) {
		errors.email = 'Email field is required';
	} else if (!Validator.isEmail(data.email)) {
		errors.email = 'Email is invalid';
	}

	const passwordErrors = validatePasswords(data);

	errors = {...errors, ...passwordErrors};

	return {
		errors,
		isValid: isEmpty(errors)
	};
};

function changeUserInfo(data) {
	const allRequired = false;
	const errors = validateUserInfo(data,allRequired);
	return {
		errors,
		isValid: isEmpty(errors)
	};
};

function validateUserInfo(data,allRequired){
	const errors = {};

	// Convert empty fields to an empty string so we can use validator functions
	data.name = !isEmpty(data.name) ? data.name : '';
	data.email = !isEmpty(data.email) ? data.email : '';

	// Name checks
	if (allRequired && Validator.isEmpty(data.name)) {
		errors.name = 'Name field is required';
	}

	// Email checks
	if (allRequired && Validator.isEmpty(data.email)) {
		errors.email = 'Email field is required';
	} else if (!Validator.isEmail(data.email)) {
		errors.email = 'Email is invalid';
	}
	// Email checks
	const emailIsEmpty = Validator.isEmpty(data.email);
	if (allRequired && emailIsEmpty) {
		errors.email = 'Email field is required';
	} else if (!emailIsEmpty && !Validator.isEmail(data.email)) {
		errors.email = 'Email is invalid';
	}
	return errors;
}

function validatePasswords(data){
	const errors = {};

	// Convert empty fields to an empty string so we can use validator functions
	data.password = !isEmpty(data.password) ? data.password : '';
	data.password2 = !isEmpty(data.password2) ? data.password2 : '';

	// Password checks
	if (Validator.isEmpty(data.password)) {
		errors.password = 'Password field is required';
	}

	if (Validator.isEmpty(data.password2)) {
		errors.password2 = 'Confirm password field is required';
	}

	if (!Validator.isLength(data.password, { min: 6, max: 30 })) {
		errors.password = 'Password must be at least 6 characters';
	}

	if (!Validator.equals(data.password, data.password2)) {
		errors.password2 = 'Passwords must match';
	}
	return errors;
}

function feedback(data){
	const errors = {};

	data.type = !isEmpty(data.type) ? data.type : '';
	data.description = !isEmpty(data.description) ? data.description : '';
	data.userId = !isEmpty(data.userId) ? data.userId : '';

	if (Validator.isEmpty(data.type)) {
		errors.type = 'Type field is required';
	}

	if (Validator.isEmpty(data.description)) {
		errors.description = 'Description field is required';
	}

	if (Validator.isEmpty(data.userId)) {
		errors.userId = 'userId field is required';
	}
	return {
		errors,
		isValid: isEmpty(errors)
	};
}

module.exports = {
	loginInput,
	registerInput,
	changePasswordInput,
	changeUserInfo,
	feedback
};
