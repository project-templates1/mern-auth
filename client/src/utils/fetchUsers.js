import {fetchUsersPending, fetchUsersSuccess, fetchUsersError} from '../actions/userActions';
import axios from 'axios';

function fetchUsers(ids) {
    return dispatch => {
        console.log(ids);
        dispatch(fetchUsersPending());
        axios.get(`/api/users/getUsers/${ids.join(',')}`)
            .then(res => {
                if(res.error) {
                    throw(res.error);
                }
                dispatch(fetchUsersSuccess(res.data));
                return res.users;
            })
            .catch(error => {
                dispatch(fetchUsersError(error));
            });
    }
}

export {
    fetchUsers
}
