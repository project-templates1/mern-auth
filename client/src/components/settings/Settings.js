import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { changePassword, changeUserInfo } from "../../actions/authActions";
import classnames from "classnames";
import { fetchUsers } from '../../utils/fetchUsers';
import LoadingSpinner from '../layout/LoadingSpinner';

function cbMessage (response){
	alert(response === true ? 'Success!' : response);
}

class Settings extends Component {
	constructor(props) {
		super(props);

		this.state = {
			password: '',
			password2: '',
			name: '',
			email: '',
			address: '',
			pending: true,
			errors: {}
		};
	}

	componentWillMount(){
		console.log('componentWillMount - this.props: ',this.props);
		const { fetchUsers } = this.props;
		fetchUsers([this.props.auth.user.id]);
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.errors) {
			this.setState({
				errors: nextProps.errors
			});
		}

		if (!nextProps.users.pending) {
			this.setState({
				name: nextProps.users.users[0].name,
				email: nextProps.users.users[0].email,
				address: nextProps.users.users[0].address,
				pending: nextProps.users.pending
			});
		}
	}

	onChange = e => {
		this.setState({ [e.target.id]: e.target.value });
	};

	onSubmit = e => {
		e.preventDefault();

		const user = {
			id: this.props.auth.user.id,
			password: this.state.password,
			password2: this.state.password2
		};

		this.props.changePassword(user, this.props.history, cbMessage);
	};

	handleInfoSubmit = e => {
		e.preventDefault();

		const user = {
			id: this.props.auth.user.id,
			name: this.state.name,
			email: this.state.email,
			address: this.state.address
		};

		this.props.changeUserInfo(user, this.props.auth, cbMessage);
	};

	render() {
		const { errors } = this.state;

		if (this.state.pending) return <LoadingSpinner/>;

		return (
			<div className="container">
				<div className="row">
					<div className="col s8 offset-s2">
						<Link to="/Dashboard" className="btn-flat waves-effect">
							<i className="material-icons left">keyboard_backspace</i> Back to
							Dashboard
						</Link>
						<div className="col s12">
							<h4>
								<b>{ this.state.name } Info</b>
							</h4>
						</div>
						<div className="col s12">
							<h5>
							 <b>User Info</b>
							</h5>
						</div>
						<form noValidate onSubmit={this.handleInfoSubmit}>
							<div className="input-field col s12">
								<input
									onChange={this.onChange}
									value={this.state.name}
									error={errors.name}
									id="name"
									type="text"
									className={classnames("", {
										invalid: errors.name
									})}
								/>
								<label htmlFor="name">Name</label>
								<span className="red-text">{errors.password}</span>
							</div>
							<div className="input-field col s12">
								<input
									onChange={this.onChange}
									value={this.state.email}
									error={errors.email}
									id="email"
									type="text"
									className={classnames("", {
										invalid: errors.email
									})}
								/>
								<label htmlFor="email">Email</label>
								<span className="red-text">{errors.email}</span>
							</div>
							<div className="input-field col s12">
								<input
									placeholder="Where you want to be: New York, the Moon, home..."
									onChange={this.onChange}
									value={this.state.address}
									error={errors.address}
									id="address"
									type="text"
									className={classnames("", {
										invalid: errors.address
									})}
								/>
								<label htmlFor="address">address</label>
								<span className="red-text">{errors.address}</span>
							</div>
							<div className="col s12">
								<button type="submit" >
									Change
								</button>
							</div>
						</form>
						<div className="col s12">
							<h5>
							 <b>Change Password</b>
							</h5>
						</div>
						<form noValidate onSubmit={this.onSubmit}>
							<div className="input-field col s12">
								<input
									onChange={this.onChange}
									value={this.state.password}
									error={errors.password}
									id="password"
									type="password"
									className={classnames("", {
										invalid: errors.password
									})}
								/>
								<label htmlFor="password">New Password</label>
								<span className="red-text">{errors.password}</span>
							</div>
							<div className="input-field col s12">
								<input
									onChange={this.onChange}
									value={this.state.password2}
									error={errors.password2}
									id="password2"
									type="password"
									className={classnames("", {
										invalid: errors.password2
									})}
								/>
								<label htmlFor="password2">Confirm New Password</label>
								<span className="red-text">{errors.password2}</span>
							</div>
							<div className="col s12">
								<button type="submit" >
									Change
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		);
	}
}

// Define types of what was passed in from the Redux store
Settings.propTypes = {
	changePassword: PropTypes.func.isRequired,
	changeUserInfo: PropTypes.func.isRequired,
	fetchUsers: PropTypes.func.isRequired,
	auth: PropTypes.object.isRequired,
	errors: PropTypes.object.isRequired,
	users: PropTypes.object
};

// Pass in data from Redux store
const mapStateToProps = state => ({
	auth: state.auth,
	errors: state.errors,
    users: state.users
});

const mapDispatchToProps = dispatch => bindActionCreators({
	changeUserInfo,
	changePassword,
    fetchUsers
}, dispatch);

// Add props, methods, withRouter as part of export
export default connect(
 	mapStateToProps,
 	mapDispatchToProps
)(withRouter(Settings));
