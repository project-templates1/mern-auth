import React, { Component } from "react";

class LoadingSpinner extends Component {

	render() {
		return (
			<div className="container valign-wrapper">
				<div className="row">
					<div className="col s12 center-align">
                		<i className="material-icons">hourglass_empty</i>
                		Loading
                	</div>
                </div>
            </div>
		);
	}
}

export default LoadingSpinner;
