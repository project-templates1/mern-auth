import { FETCH_USERS_PENDING, FETCH_USERS_SUCCESS, FETCH_USERS_ERROR } from './types';

function fetchUsersPending() {
    console.log('fetchUsersPending');
    return {
        type: FETCH_USERS_PENDING
    }
}

function fetchUsersSuccess(users) {
    console.log('fetchUsersSuccess',users);
    return {
        type: FETCH_USERS_SUCCESS,
        users: users
    }
}

function fetchUsersError(error) {
    console.log('fetchUsersError',error);
    
    return {
        type: FETCH_USERS_ERROR,
        error: error
    }
}

export {
    fetchUsersPending,
    fetchUsersSuccess,
    fetchUsersError
}