const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const FeedbackSchema = new Schema({
	userId: {
		type: Schema.Types.ObjectId,
		required: true
	},
	type: {
		type: String,
		enum: ['BUG', 'RECOMMENDATION', 'COMPLIMENT'],
		required: true
	},
	description: {
		type: String,
		required: true
	},
	createdDate: {
		type: Date,
		default: Date.now
	}
});

module.exports = Feedback = mongoose.model("feedback", FeedbackSchema);
