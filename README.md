# mern-auth

![Final App](https://i.postimg.cc/tybZb8dL/final-MERNAuth.gif)
Minimal full-stack MERN app with authentication using passport and JWTs.

This project uses the following technologies:

- [React](https://reactjs.org) and [React Router](https://reacttraining.com/react-router/) for frontend
- [Express](http://expressjs.com/) and [Node](https://nodejs.org/en/) for the backend
- [MongoDB](https://www.mongodb.com/) for the database
- [Redux](https://redux.js.org/basics/usagewithreact) for state management between React components

## Configuration

Make sure to add your `database` is setup appropriate for your MongoDB config and your create your own unique `secret` in`config/keys.js`.

```javascript
module.exports = {
  database: "mongodb://localhost/node-auth",
  secretOrKey: "secret"
};
```

## Quick Start
- Install Mongo: https://docs.mongodb.com/manual/administration/install-community/
- Install Express Generator: `npm install express-generator -g`

```javascript
// Install dependencies for server & client
npm install && npm run client-install

// Run client & server with concurrently
npm run dev

// Server runs on http://localhost:5000 and client on http://localhost:3000
```

This is based on https://github.com/rishipr/mern-auth/
