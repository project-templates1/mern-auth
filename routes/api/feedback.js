const express = require('express');
const router = express.Router();
const passport = require('passport');

// Load input validation
const validator = require('../../services/validator');

// Load Feedback model
const Feedback = require('../../models/Feedback');

// Ensure all routes are private
router.use(passport.authenticate('jwt', { session: false}));

router.post('/submitFeedback', (req, res) => {

	// Form validation
	const { errors, isValid } = validator.feedback(req.body);

	// Check validation
	if (!isValid) {
		return res.status(400).json(errors);
	}

	const newFeedback = new Feedback(req.body);

	newFeedback.save().then(feedback => {
		return res.status(200).json({ msg: 'Thank you for your input! It is much appreciated' });
	})
	.catch(err =>{
		return res.status(400).json({ msg: err });
	})
});

module.exports = router;